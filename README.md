# TP Redes - Backlog

## Done

* Conversor de string para binário;
* Testes do conversor;
* Sockets servidor-cliente;
* Testes do socket;
* Unir socket e conversor;

## To do

* Aaptar sockets para usar bitset ao invés de strings;
* Elaborar simulação;
* ...

### Changelog:

* Augusto, 9/11
* Augusto, 9/11
