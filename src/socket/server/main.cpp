#include "../ServerSocket.h"
#include "../SocketException.h"
#include <iostream>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

int main (int argc, char *argv[]) {
    std::cout << "Rodando..." << std::endl;
    try {
        // Cria o socket
        ServerSocket server (30000);

        while (true) {
            ServerSocket new_sock;
            server.accept(new_sock);
            try { 
                while (true) { 
                    std::string data;
                    new_sock >> data;
                    new_sock << data;
                }
            }
            catch (SocketException&) {}
        }
    }
    catch (SocketException& e) {
        std::cout << "Excecao capturada: " << e.description() << std::endl << "Saindo." << std::endl;
    }
    
    return 0;
}

#pragma clang diagnostic pop