#include "../ClientSocket.h"
#include "../SocketException.h"
#include <iostream>
#include <string>

int main (int argc, char *argv[]) {
    try {
        ClientSocket client_socket ("localhost", 30000);

        std::string message, reply;
        while (message != "sair") {
            try {
                std::cout << "Digite uma mensagem: " << std::endl;
                std::cin >> message;
                client_socket << message;
                client_socket >> reply;
            }
            catch (SocketException &) {}

            std::cout << "Resposta recebida do servidor:" << std::endl
                      << "\"" << reply << "\"" << std::endl;
        }
    }
    catch (SocketException& e) {
        std::cout << "Excecao capturada:" << e.description() << std::endl;
    }
}
