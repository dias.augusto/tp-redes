// Implementacao da classe ServerSocket

#include <iostream>
#include "ServerSocket.h"
#include "SocketException.h"


ServerSocket::ServerSocket (int port) {
    if (!Socket::create()) {
        throw SocketException ("Nao foi possivel criar um socket de servidor.");
    }

    if (!Socket::bind (port)) {
        throw SocketException ("Nao foi possivel se conectar a porta.");
    }

    if (!Socket::listen()) {
        throw SocketException ("Nao foi possivel monitorar o socket.");
    }
}

ServerSocket::~ServerSocket() {}


const ServerSocket& ServerSocket::operator << (const std::string& s) const {
    if (!Socket::send(s)) {
        throw SocketException ("Nao foi possivel escrever para o socket.");
    }
    std::cout << "Resposta enviada." << std::endl;
    return *this;
}


const ServerSocket& ServerSocket::operator >> (std::string& s) const {
    if (!Socket::recv(s)) {
        throw SocketException("Nao foi possivel receber do socket.");
    }
    std::cout << "Mensagem recebida." << std::endl;
    return *this;
}

void ServerSocket::accept (ServerSocket& sock) {
    if (!Socket::accept (sock)) {
        throw SocketException ("Nao foi possivel aceitar o socket.");
    }
    std::cout << "Novo host conectado." << std::endl;
}
