#include <bitset>
#include <iostream>

#define word 8
#define page 4

bitset <page> package(bitset <word> &msg, int beg) {
    bitset <page> pack;

    for(int i = beg, j = 0; i < beg + page; i++) {
        pack[j++] = msg[i];
    }

    return pack;
}
