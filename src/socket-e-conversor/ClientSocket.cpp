// Implementacao da classe ClientSocket

#include "ClientSocket.h"
#include "SocketException.h"


ClientSocket::ClientSocket (std::string host, int port) {

    if (!Socket::create()) {
        throw SocketException ("Nao foi possivel criar socket cliente.");
    }

    if (!Socket::connect (host, port)) {
        throw SocketException ("Nao foi possivel conectar a porta.");
    }
}


const ClientSocket& ClientSocket::operator << (const std::string& s) const {
    if (!Socket::send (s)) {
        throw SocketException ("Nao foi possivel escrever para o socket.");
    }

    return *this;
}


const ClientSocket& ClientSocket::operator >> (std::string& s) const {
    if (!Socket::recv (s)) {
        throw SocketException ("Nao foi possivel ler do socket.");
    }

    return *this;
}
