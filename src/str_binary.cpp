#include <bitset>
#include <iostream>

#define word 8
#define page 4

using namespace std;

bitset <page> package(bitset <word> &msg, int beg) {
	bitset <page> pack;
	
	for(int i = beg, j = 0; i < beg + page; i++) {
		pack[j++] = msg[i];
	}
	
	return pack;
}

int main() {
	
	char test[16] = "Hello World! =D";
	
	bitset <word> msg;
	bitset <page> pack1;
	bitset <page> pack2;
	
	cout<<msg<<endl<<endl;
	
	int i = 0;
	
	while(test[i] != '\0') {
		msg = test[i++];
		pack1 = package(msg, 4);
		pack2 = package(msg, 0);
		
		cout<<pack1<<" + "<<pack2<<" = ";
		
		cout<<msg<<" = "<<(char)msg.to_ulong()<<endl;
	}
	
	return 0;
}